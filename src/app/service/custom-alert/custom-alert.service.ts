import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CustomAlertService {

  constructor(private alert: AlertController) { }

  async presentAlert(header: string, message?: string) {
    const alert = await this.alert.create({
      // cssClass: 'alert-danger',
      header,
      message,
      buttons: ['OK']
    });

    await alert.present();
  }

  // async presentAlertConfirm(message: string) {
  //   const alert = await this.alert.create({
  //     message: message,
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         cssClass: 'secondary',
  //         handler: (blah) => {
  //           console.log('Confirm Cancel: blah');
  //         }
  //       }, {
  //         text: 'Okay',
  //         handler: () => {
  //           console.log('Confirm Okay');
  //         }
  //       }
  //     ]
  //   });

  //   await alert.present();
  // }


  async ErrorAlert(header: string, message: string) {
    const alert = await this.alert.create({
      cssClass: 'alert-danger',
      header: header,
      subHeader: 'Please contact your school for assistance.',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  // //*** old parent */
  // alertMessage(title: string, message: string, iconClass: string = "closeImg") {
  //   this.alertItem = this.alert.create({
  //     title: `<div class="${iconClass} pull-left"></div><div class="pull-left margin-left-10 font-size-16">${title}</div>`,
  //     message: message,
  //     cssClass: "alert-danger",
  //     buttons: ["OK"]
  //   });
  //   this.alertItem.present();
  // }



}
