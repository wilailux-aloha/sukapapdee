import { Injectable } from '@angular/core';
import { Config_URL } from 'src/app/config';
import axios from 'axios';
import { ApiproviderService } from './provider/apiprovider.service';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  private static provideAPI;
  public api = Config_URL;
  private instance: any;

  constructor() { }

  public _axios() {
    if (!this.instance) {
        this.instance = axios.create({
            baseURL: this.api,
            timeout: 30000,
        });
    }
    // this.instance.defaults.headers = this.getHeader();
    return this.instance;
  }

  public getHeader(token){
    const headers = {
      authorization: 'Bearer ' + token,
      'Content-Type' : 'application/json'
    }
    return headers;
  }

  public getParentAPI(): ApiproviderService {
    if (RequestService.provideAPI == null) {
        RequestService.provideAPI = new ApiproviderService(this._axios());
    }
    return RequestService.provideAPI;
  }

}
