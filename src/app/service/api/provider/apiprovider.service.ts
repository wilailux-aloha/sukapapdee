import { Injectable } from '@angular/core';
import { RouteOption, Routename } from 'src/app/models/route.model';

// @Injectable({
//   providedIn: 'root'
// })
export class ApiproviderService {
  static RouteOption  = RouteOption;
  static Routename = Routename;

  constructor(
    private axios
    ) { }

    public post  = (route: string , body : object, options : object ) => new Promise(async (resolve, reject) => {
      this.axios.defaults.headers = options;
      this.axios.post(route,  body , options)
        .then(res => {
          resolve(res.data);
        }).catch((error) => {
          reject({
            error: true,
            message: error
          });
        });
    })

    public get  = (route: string , param : string , options : object ) => new Promise(async (resolve, reject) => {
      this.axios.get(route + param,  options)
        .then(res => {
          resolve(res.data);
        }).catch((error) => {
          reject({
            error: true,
            message: error
          });
        });
     })

}
