import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(public storage: Storage) { }

  public setStorage(key: string, value: any) {
    return this.storage.set(key, value);
  }

  public getStorage(key: string) {
    return this.storage.get(key);
  }

  public clearStorage() {
    return this.storage.clear();
  }

  public removeStorage(key: string) {
    return this.storage.remove(key);
  }


}
