import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CustomLoadingService {
  public loaderItem: any = null;

  constructor(
    private loader: LoadingController
  ) { }

  // loading(message: string) {
  //   this.loaderItem = this.loader.create({
  //     message: message
  //   });
  //   this.loaderItem.present();
  // }

  async loading(message: string) {
    this.loaderItem = await this.loader.create({
      cssClass: 'my-custom-class',
      message: message,
      duration: 2000
    });
    await this.loaderItem.present();

  }

  async loaded() {
    if (this.loaderItem != null) {
      await this.loaderItem.dismiss();
      this.loaderItem = null;
    }
  }

  // async presentLoading() {
  //   const loading = await this.loadingController.create({
  //     cssClass: 'my-custom-class',
  //     message: 'Please wait...',
  //     duration: 2000
  //   });
  //   await loading.present();

  //   const { role, data } = await loading.onDidDismiss();
  //   console.log('Loading dismissed!');
  // }

  // async presentLoadingWithOptions() {
  //   const loading = await this.loadingController.create({
  //     spinner: null,
  //     duration: 5000,
  //     message: 'Click the backdrop to dismiss early...',
  //     translucent: true,
  //     cssClass: 'custom-class custom-loading',
  //     backdropDismiss: true
  //   });
  //   await loading.present();

  //   const { role, data } = await loading.onDidDismiss();
  //   console.log('Loading dismissed with role:', role);
  // }



}
