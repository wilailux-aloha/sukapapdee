import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { StorageService } from '../storage/storage.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    public auth: AuthService,
    public router: Router,
    public storage: StorageService
  ) { }

  async canActivate(): Promise<boolean> {
    const token = await this.auth.isAuthenticated();
    console.log('passRegister, token:', token);

    if(!token){
      this.router.navigate(['after-splash-screen']);
      return false;
    }
    return true;
  }
}
