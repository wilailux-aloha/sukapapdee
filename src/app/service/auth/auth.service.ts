import { Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public storage: StorageService) { }

  public async isAuthenticated(): Promise<boolean>{
    const userProfile = await this.storage.getStorage('token');
    if (userProfile){
      return true;
    }
    // return !this.jwtHelper.isTokenExpired(token);
    return false;

  }

}
