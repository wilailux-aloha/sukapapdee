import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiproviderService } from 'src/app/service/api/provider/apiprovider.service';
import { RequestService } from 'src/app/service/api/request.service';
import { CustomAlertService } from 'src/app/service/custom-alert/custom-alert.service';
import { StorageService } from 'src/app/service/storage/storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;
  isSubmitted = false;

  constructor(
    public storage: StorageService,
    public router: Router,
    private alertService: CustomAlertService,
    private requestService: RequestService,
    public formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    console.log('ngOnInit RegisterPage');
    this.initregisterForm();
  }

  initregisterForm(){
    console.log('initregisterForm');
    this.registerForm = this.formBuilder.group({
      email: ['',
               [Validators.required,
                Validators.pattern( /^(([^<>()\[\]\\.,;:\s@]+(\.[^<>()\[\]\\.,;:\s@]+)*)|(.+))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
              ]
            ],
    });
  }

  get errorControl() {
    return this.registerForm.controls;
  }

  nextStep(){
    this.isSubmitted = true;
    console.log('email:', this.registerForm);
    if (!this.registerForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    }else {
      console.log(this.registerForm.value.email);
      let service = this.requestService.getParentAPI();
      ApiproviderService.RouteOption.emailVerify.email = this.registerForm.value.email;
      service.post(ApiproviderService.Routename.emailVerify, ApiproviderService.RouteOption.emailVerify, {})
      .then(res => this.EmailVerifySuccess(this.registerForm.value.email, res))
      .catch(err => this.alertService.presentAlert('Invalid Email Address:', err.message.message));
    }
  }

  async EmailVerifySuccess(email, data){
    console.log('EmailVerifySuccess:', data);
    if (data.token === ''){this.alertService.presentAlert('EmailVerify:', 'Invalid Email Address'); }
    await this.storage.setStorage('token', data.token); // set token for createApollo
    await this.storage.setStorage('parent_email', email);
    this.router.navigate(['home']);
  }

}
