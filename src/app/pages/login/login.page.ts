import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiproviderService } from 'src/app/service/api/provider/apiprovider.service';
import { RequestService } from 'src/app/service/api/request.service';
import { CustomAlertService } from 'src/app/service/custom-alert/custom-alert.service';
import { CustomLoadingService } from 'src/app/service/custom-loading/custom-loading.service';
import { StorageService } from 'src/app/service/storage/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public loginForm: FormGroup;
  public isSubmitted = false;

  constructor(
    public formBuilder: FormBuilder,
    private requestService: RequestService,
    private alertService: CustomAlertService,
    public router: Router,
    public storage: StorageService,
    public loadService: CustomLoadingService,
  ) { }

  ngOnInit() {
    console.log('ngOnInit LoginPage');
    this.initLoginForm();
  }

  initLoginForm(){
    console.log('initLoginForm');
    this.loginForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      email: ['',
               [Validators.required,
                Validators.pattern( /^(([^<>()\[\]\\.,;:\s@]+(\.[^<>()\[\]\\.,;:\s@]+)*)|(.+))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
              ]
            ],
    });
  }

  get errorControl() {
    return this.loginForm.controls;
  }

  logIn() {
    this.isSubmitted = true;
    if (!this.loginForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      this.loadService.loading('Loading...')
      console.log(this.loginForm.value);
      const { email, password } = this.loginForm.value;
      let service = this.requestService.getParentAPI();
      ApiproviderService.RouteOption.login.username = email
      ApiproviderService.RouteOption.login.password = password
      service.post(ApiproviderService.Routename.login, ApiproviderService.RouteOption.login, {})
      .then(res => this.signInSuccess(res, password))
      .catch(err => this.signInError(err))
    }
  }

  async signInSuccess(data, password){
    data['user'].password = password
    console.log('signInSuccess:', data['user']);
    await this.loadService.loaded()
    await this.storage.setStorage('userProfile', data['user'])
    await this.storage.setStorage('token', data['token'])
    this.router.navigate(['home']);

  }

  async signInError(err) {
    await this.loadService.loaded()
    console.log('signInError:', err);
    this.alertService.presentAlert('signIn:', 'Invalid username or password.');
  }

  register(){
    console.log('go to register:');
    this.router.navigate(['register']);
  }


}
