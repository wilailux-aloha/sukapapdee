import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-after-splash-screen',
  templateUrl: './after-splash-screen.page.html',
  styleUrls: ['./after-splash-screen.page.scss'],
})
export class AfterSplashScreenPage implements OnInit {

  constructor(
    public router: Router,
  ) { }

  ngOnInit() {
  }

  logIn(){
    this.router.navigate(['login']);
  }

  register(){
    this.router.navigate(['register']);
  }

}
